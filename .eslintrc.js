module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    webextensions: true,
  },
  extends: [
    'airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-plusplus': 'off',
    'no-restricted-syntax': ['off'],
    'no-param-reassign': ['off'],
  },
};
