import DeezerApi from './api/deezer';

const deezer = new DeezerApi();

function getCurrentTrack() {
  return {
    artist: document.querySelector('[itemprop="byArtist"]').innerText.trim(),
    title: document.querySelector('.header-new-title').innerText.trim(),
  };
}

async function run() {
  try {
    const song = getCurrentTrack();
    const response = await deezer.search(song);
    const trackId = response.data[0].id;

    const headerEl = document.querySelector('.header-new-content');

    const iframe = document.createElement('iframe');
    iframe.style.marginTop = '1em';
    iframe.scrolling = 'no';
    iframe.frameBorder = '0';
    iframe.allowTransparency = true;
    iframe.src = `https://www.deezer.com/plugins/player?format=classic&autoplay=false&playlist=false&width=700&height=350&color=ff0000&layout=dark&size=medium&type=tracks&id=${trackId}&app_id=1`;
    iframe.width = '700';
    iframe.height = '92';
    headerEl.appendChild(iframe);
  } catch (error) {
    console.log(error);
  }
}


function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(run);
