import { basicRequestBackend } from './api/utils';

browser.runtime.onMessage.addListener(async (message, sender) => {
  if (sender.id === browser.runtime.id) {
    if (message.command === 'request') {
      const response = await basicRequestBackend(message.request);
      return response;
    }
  }
  return null;
});
