import { basicRequest } from './utils';

export default class DeezerApi {
  constructor() {
    this.baseUrl = 'https://api.deezer.com/';
  }

  /**
   *
   * @param {string} route
   * @param {Object.<string, any>} params
   * @param {'get'|'post'|'delete'} method
   */
  request(route, params, method = undefined) {
    Object.assign(params, {
      output: 'json',
    });
    return basicRequest({
      baseUrl: this.baseUrl + route,
      params,
      method,
    });
  }

  search({ artist, title }) {
    return this.request('search', {
      q: `artist:"${artist}" track:"${title}"`,
    });
  }
}
