function getProxyUrl(url) {
  return `https://mS2UQHeOtWc-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=${encodeURIComponent(url)}`;
}

// TODO: Add user agent
export async function basicRequestBackend({ baseUrl, params, method = 'get' }) {
  const url = new URL(baseUrl);
  url.search = new URLSearchParams(params);
  const proxiedUrl = getProxyUrl(url);
  console.log(proxiedUrl);
  const response = await fetch(proxiedUrl, { method });
  console.log(response);
  const data = await response.json();
  console.log(data);
  return data;
}

export function basicRequest(request) {
  return browser.runtime.sendMessage({
    command: 'request',
    request,
  });
}
