import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import copy from 'rollup-plugin-copy';

export default [
  {
    input: 'src/main.js',
    output: {
      file: 'dist/main.js',
      format: 'iife',
      sourcemap: true,
    },
    plugins: [
      nodeResolve(),
      commonjs(),
      copy({
        targets: [
          'src/manifest.json',
        ],
        outputFolder: 'dist',
      }),
    ],
  },
  {
    input: 'src/background.js',
    output: {
      file: 'dist/background.js',
      format: 'iife',
      sourcemap: true,
    },
    plugins: [
      nodeResolve(),
      commonjs(),
    ],
  },
];
